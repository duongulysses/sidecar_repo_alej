package com.example.hellomongodb.Config;

import com.example.hellomongodb.domain.Employee;
import lombok.AllArgsConstructor;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.step.builder.SimpleStepBuilder;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class MyEmployeeStepBuilder extends MyStepBuilder<Employee, Employee> {
    private StepBuilderFactory stepBuilderFactory;

    @Override
    public SimpleStepBuilder<Employee, Employee> getChunk(String stepName, int size) {
        return stepBuilderFactory.get(stepName).chunk(size);
    }
}
