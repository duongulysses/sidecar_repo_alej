package com.example.hellomongodb.Config;

import com.example.hellomongodb.domain.Employee;
import org.springframework.batch.item.data.MongoItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;

import java.util.HashMap;

@Component
public class EmployeeReaderBuilder extends MyItemReaderBuilder<Employee> {
    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public MongoItemReader<Employee> reader() {
        MongoItemReader<Employee> reader = new MongoItemReader<>();
        reader.setTemplate(mongoTemplate);
        reader.setSort(new HashMap<String, Sort.Direction>() {{
            put("_id", Sort.Direction.ASC);
        }});
        reader.setTargetType(Employee.class);
        reader.setQuery("{}");
        return reader;
    }
}
