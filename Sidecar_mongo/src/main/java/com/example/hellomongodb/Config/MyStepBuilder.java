package com.example.hellomongodb.Config;

import org.springframework.batch.core.step.builder.SimpleStepBuilder;

public abstract class MyStepBuilder<IN, OUT> {
    public abstract SimpleStepBuilder<IN, OUT> getChunk(String stepName, int size);
}
