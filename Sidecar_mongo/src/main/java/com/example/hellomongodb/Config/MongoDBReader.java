package com.example.hellomongodb.Config;

import com.example.hellomongodb.domain.Employee;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableBatchProcessing
@Slf4j
public class MongoDBReader {

    private final JobBuilderFactory jobBuilderFactory;
    private final MyItemWriterBuilder<Employee> writer;
    private final MyItemReaderBuilder<Employee> reader;
    private final MyStepBuilder<Employee, Employee> stepBuilder;
    @Value("${spring.my.custom.myExportCsvFile}")
    private String csvPath;

    @Autowired
    public MongoDBReader(JobBuilderFactory jobBuilderFactory, MyItemWriterBuilder<Employee> writer, MyItemReaderBuilder<Employee> reader, MyStepBuilder<Employee, Employee> stepBuilder) {
        this.jobBuilderFactory = jobBuilderFactory;
        this.writer = writer;
        this.reader = reader;
        this.stepBuilder = stepBuilder;
    }

    @Bean
    public Job readEmployee() {
        return jobBuilderFactory.get("readEmployee").flow(step1()).end().build();
    }

    @Bean
    public Step step1() {
        return stepBuilder.getChunk("step1", 10)
                .reader(reader.reader())
                .writer(writer.writer(csvPath + "/employees.csv"))
                .build();
    }

}
