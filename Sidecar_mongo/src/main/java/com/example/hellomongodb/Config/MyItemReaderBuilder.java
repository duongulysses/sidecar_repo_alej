package com.example.hellomongodb.Config;

import org.springframework.batch.item.data.MongoItemReader;


public abstract class MyItemReaderBuilder<IN> {
    public abstract MongoItemReader<IN> reader();
}
